1. Clone repo https://bitbucket.org/tanquyvt/bookstore-dockerized/src/master/

2. Setup environment

	docker-compose up -d

3. Run the test by access https://localhost/app1

(Optional) Access phpmyadmin http://localhost:8081 using username/password: root/P@ssw0rd

